const server = require("../server");
const http = require("http");
const assert = require("assert");

describe("Testing server...", function () {
  before(function () {
    server.listen(3000);
  });

  describe("/", function () {
    it("should return 200", function (done) {
      http.get("http://localhost:3000", function (res) {
        assert.equal(200, res.statusCode);
        done();
      });
    });

    it('should say "Hello, world!"', function (done) {
      http.get("http://localhost:3000", function (response) {
        var data = "";

        response.on("data", function (chunk) {
          data += chunk;
        });

        response.on("end", function () {
          assert.equal("Hello, World", data);
          done();
        });
      });
    });
  });

  describe("/api", function () {
    it("should return 200", function (done) {
      http.get("http://localhost:3000/api", function (res) {
        assert.equal(200, res.statusCode);
        done();
      });
    });
  });

  after(function () {
    server.close();
  });
});
