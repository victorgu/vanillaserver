# vanilla server

## A web server created with http module

- Language: Javascript
- Framework: No framework (http module)
- Dependencies: Just mocha to run the tests.

To run the project you just need to have Node (version 12.14ˆ):
\$ yarn start
