const http = require("http");
const data = require("./data.json");

this.server = http.createServer(function (request, response) {
  switch (request.url) {
    case "/":
      response.writeHead(200, { "Content-Type": "text/plain" });
      response.end("Hello, World");
      break;
    case "/api":
      if (data) {
        response.writeHead(200, { "Content-Type": "application/json" });
        response.end(JSON.stringify(data));
      }
      break;
    default:
      break;
  }
});

exports.listen = function () {
  this.server.listen.apply(this.server, arguments);
};

exports.close = function (callback) {
  this.server.close(callback);
};
